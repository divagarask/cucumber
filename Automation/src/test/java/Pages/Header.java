package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.List;

public class Header extends Base
{

    public Header(WebDriver driver) {
        super();
    }
    By header = By.xpath("//*[@id='masthead-bar-one']//following::ul[@data-testid='mini-nav'][2]//li[@data-testid='mini-nav-item']");
    By Health = By.xpath("//*[@id='masthead-bar-one']//following::ul[@data-testid='mini-nav'][2]//li[@data-testid='mini-nav-item']/a[text()='Health']");
    By HomeIcon = By.xpath("//a[@aria-label='New York Times Logo. Click to visit the homepage']");
    By Science = By.xpath("//*[@id='masthead-bar-one']//following::ul[@data-testid='mini-nav'][2]//li[@data-testid='mini-nav-item']/a[text()='Science']");


    public List<String> headervalues() throws InterruptedException {

        List<String> actual=new ArrayList();
        Thread.sleep(3000);
        List<WebElement> headerlist= driver.findElements(header);
        System.out.println("size: "+headerlist.size());
        for(int i=0;i<headerlist.size();i++)
        {
            actual.add(i, headerlist.get(i).getText().toString());
        }
        return actual;
    }

    public void clickOnHealth() {
        driver.findElement(Health).click();

    }

    public void clickOnHomeicon(){
        driver.findElement(HomeIcon).click();
    }

    public void clickOnScience()
    {
        driver.findElement(Science).click();
    }

}
