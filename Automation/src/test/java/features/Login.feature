Feature: DVT Assignment

  Scenario: DVT script
    Given I launch nytimes
      |https://www.nytimes.com/|
    And I Verify the title
    |The New York Times - Breaking News, US News, World News and Videos|
    When I Verify the headers
    |World|
    |U.S.   |
    |Politics|
    |N.Y.|
    |Business|
    |Opinion|
    |Tech|
    |Science|
    |Health|
    |Sports|
    |Arts|
    |Books|
    |Style|
    |Food|
    |Travel|
    |Magazine|
    |T Magazine|
    |Real Estate|
    |Video      |
    Then I Click on Health header and verify Title
    |Health - The New York Times|
    And I navigate to home page
    |The New York Times - Breaking News, US News, World News and Videos|
    Then I Click on Science header and verify Title
    |Science - The New York Times|
