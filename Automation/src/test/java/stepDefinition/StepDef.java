package stepDefinition;


import Pages.Base;
import Pages.Header;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.bs.A;
import io.cucumber.java.en.*;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.*;
import org.junit.Assert;

import java.io.IOException;
import java.util.List;

public class StepDef extends Base{

    private Base base;
    public StepDef( Base base)
    {
        this.base=base;
    }
    Header header = new Header(Base.driver);
    @Given("^I launch nytimes$")
    public void launch_webpage(DataTable data) throws Throwable{
        List<List<String>> obj = data.cells();
        System.out.println("URL "+obj.get(0).get(0));
        base.launchBrowser(obj.get(0).get(0));
       /* RestAssured.baseURI = "https://rahulshettyacademy.com";
        given().log().all().queryParam("key","qaclick123").header("Content-Type","application/json")
                .body("{\n" +
                        "  \"location\": {\n" +
                        "    \"lat\": -38.383494,\n" +
                        "    \"lng\": 33.427362\n" +
                        "  },\n" +
                        "  \"accuracy\": 50,\n" +
                        "  \"name\": \"Frontline house\",\n" +
                        "  \"phone_number\": \"(+91) 983 893 3937\",\n" +
                        "  \"address\": \"29, side layout, cohen 09\",\n" +
                        "  \"types\": [\n" +
                        "    \"shoe park\",\n" +
                        "    \"shop\"\n" +
                        "  ],\n" +
                        "  \"website\": \"http://google.com\",  \n" +
                        "  \"language\": \"French-IN\"\n" +
                        "}").when().post("maps/api/place/add/json")
                .then().log().all().statusCode(200);*/
    }
    @And("^I Verify the title$")
    public void verify_udemytitle(DataTable data) throws InterruptedException {
        List<List<String>> obj = data.cells();
        Assert.assertEquals("Title is not matching",driver.getTitle(),(obj.get(0).get(0)));
    }

    @When("^I Verify the headers$")
    public void verify_categories(DataTable data) throws InterruptedException, IOException {
        base.takescreenshot();
        List<List<String>> obj = data.cells();
        List<String> actual = header.headervalues();
        for(int i=0;i<obj.size();i++)
        {
            Assert.assertEquals(obj.get(i).get(0)+" Header is not available",actual.contains(obj.get(i).get(0)), true);

        }
    }
    @Then("^I Click on Health header and verify Title$")
    public void verify_business_catergory_title(DataTable data) throws InterruptedException, IOException {
            List<List<String>> obj = data.cells();
            header.clickOnHealth();
            base.takescreenshot();
            Assert.assertEquals("Title is not matching",driver.getTitle().contains(obj.get(0).get(0)), true);
        }
    @And("^I navigate to home page$")
    public void navigate_to_homepage(DataTable data){
        header.clickOnHomeicon();
        List<List<String>> obj = data.cells();
        Assert.assertEquals("Title ia not matching",driver.getTitle().contains(obj.get(0).get(0)), true);
    }

    @And("^I Click on Science header and verify Title$")
    public void navigate_and_verify_science_tile(DataTable data){

        header.clickOnScience();
        List<List<String>> obj = data.cells();
        Assert.assertEquals("Title is not matching",driver.getTitle().contains(obj.get(0).get(0)), true);
    }

    }
